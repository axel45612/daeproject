<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Productos</title>
        <!-- FRAMEWORK BOOTSTRAP para el estilo de la pagina-->
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="admin-content">

            <div class="title-content">
            </div>

            <div class="row-content">
                <h4 style="color: black">Lista de usuarios</h4>
                <hr/>
            </div>
            <table id="users_table" class="table table-hover ">
                <thead>
                    <tr style="color: black">
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th>Precio</th>
                        <th>Caracteristicas</th>
                        <th>Existencia</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="color: black">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <input id="clickMe" id="Editar" class="btn btn-outline-primary btn-sm" type="button" value="Editar"
                                   data-toggle="modal" data-target="#myModal2"/>
                            <form action="#" method="POST">
                                <input type="hidden" name="user_id" value="">
                                <button type="submit" class="btn btn-outline-danger btn-sm"
                                        data-toggle="modal" data-target="#myModal">Eliminar</button>
                            </form>
                        </td>
                        <% %>
                    </tr>
                </tbody>
            </table>
    <!-- Button to Open the Modal -->
<button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#myModal">
 Añadir Producto
</button>

<!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Modal Heading</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
          <form action="action">
                <label >Nombre Producto</label><br>
        <input style="width:100%" type="text"name="producto" id="producto"class="form_control"><br>
        <label >Tipo</label><br>
        <input style="width:100%" type="text"name="tipo" id="tipo"class="form_control"><br>
        <label >Precio</label><br>
        <input style="width:100%" type="text"name="precio" id="precio"class="form_control"><br>
        <label >Caracteristicas</label><br>
        <input style="width:100%" type="text"name="caracteristica" id="caracteristica"class="form_control">
              </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-success" data-dismiss="modal">Añadir Producto</button>
      </div>

    </div>
  </div>
</div>

<!-- The Modal editar -->
<div class="modal fade" id="myModal2">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Modal Heading</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
          <form action="action">
                <label >Nombre Producto</label><br>
        <input style="width:100%" type="text"name="producto" id="producto"class="form_control"><br>
        <label >Tipo</label><br>
        <input style="width:100%" type="text"name="tipo" id="tipo"class="form_control"><br>
        <label >Precio</label><br>
        <input style="width:100%" type="text"name="precio" id="precio"class="form_control"><br>
        <label >Caracteristicas</label><br>
        <input style="width:100%" type="text"name="caracteristica" id="caracteristica"class="form_control">
              </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-success" data-dismiss="modal">Editar</button>
      </div>

    </div>
  </div>
</div>                

    </body>
</html>
